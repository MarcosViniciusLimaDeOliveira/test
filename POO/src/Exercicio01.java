import java.util.Scanner;

public class Exercicio01 {

	static final double PI = 3.14;
		
	public static void main(String[] args) {
		// Declara��o e inicializa��o de variaveis
		
		double raio, area;
		
		raio = 0;
		area = 0;
		
		Scanner tecla = new Scanner (System.in);
		
		//Entrada de Dadoss 
		System.out.println("Informe o raio do circulo");
		
		raio = tecla.nextDouble();
		
		area = PI * Math.sqrt(raio);
		
		//saida de dados 
		
		System.out.println("Area do circulo �:" + area);

	}

}
